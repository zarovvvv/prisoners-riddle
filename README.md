# prisoners-riddle

this script aims to recreate the experiment from the Derek's video - https://www.youtube.com/watch?v=iSNsgj1OCLA&lc=

success will only be printed if all prisoners found their number
if they fail, nothing will be printed

in this way, you can create for loop in the terminal and output the result in text file to see how many times they succeed
example:
`for i in {1..1000}; do python prisoners-riddle.py >> test.txt; done `

and you can count the number of successes like this:
`cat test.txt | grep "sucess!" | wc -l`
