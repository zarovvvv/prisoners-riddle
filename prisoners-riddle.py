# this script aims to recreate the experiment from the Derek's video - https://www.youtube.com/watch?v=iSNsgj1OCLA&lc=

# success will only be printed if all prisoners found their number
# if they fail, nothing will be printed
# in this way, you can create for loop in the terminal and output the result in text file to see how many times they succeed
# example - for i in {1..1000}; do python prisoners-riddle.py >> test.txt; done 
# and you can count the number of successes like this - cat test.txt | grep "sucess!" | wc -l

import random
import sys

# 101 will mean 100 prisoners
prisoners = list(range(1, 101))
slips = {}
rannumlist = []
prisoner_sucess = {}


# create random numbers indide slips
# there is probabbly better way to do this
def random_num_in_slips():
    rannum =  random.randint(1, len(prisoners))
    while rannum in rannumlist:
        rannum =  random.randint(1, len(prisoners))
    rannumlist.append(rannum)
    return rannum

# adding prisoner number on random slip
for slip in prisoners:
    slips[slip] = random_num_in_slips()

# iterate each prisoner
for prisonner_num in prisoners:
    # max slips that they can open are half of the number of slips
    max_tries = len(prisoners)/2
    # open first slip and check if the number inside matches his number
    open_slip = slips.get(prisonner_num)
    if prisonner_num == open_slip:
        prisoner_sucess[prisonner_num] = True
    else:
        # continue to open boxes until prisoners find his number
        while prisonner_num != open_slip:
            max_tries -= 1
            if max_tries == 0:
                prisoner_sucess[prisonner_num] = False
                # exit program if even one prissoner fails
                sys.exit(0)
            else:
                # open new box with the same number as the number inside the previous box
                open_slip = slips.get(open_slip)
                #
                if open_slip == prisonner_num:
                    prisoner_sucess[prisonner_num] = True
                    

#print(prisoner_sucess)

print("sucess!")
